﻿using BowlReservation.ViewModels;
using FluentAssertions;
using System;
using System.Linq;
using Xunit;

namespace BowlReservation.Tests
{
    public class LaneSlotTests
    {
        [Fact]
        public void TestFactory()
        {
            LaneSlotFactory.Instance.MaxLanes = 12;

            var slots = LaneSlotFactory.Instance.Generate().ToArray();
            slots.Length.Should().Be(12);

            slots = LaneSlotFactory.Instance.Generate(LaneSlotFactoryRules.Half).ToArray();
            slots.Length.Should().Be(6);
        }
    }
}