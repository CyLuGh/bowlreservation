﻿using BowlReservation.ViewModels;
using FluentAssertions;
using System;
using System.Linq;
using Xunit;

namespace BowlReservation.Tests
{
    public class TimeSlotTests
    {
        [Fact]
        public void TestFactory()
        {
            // Generate time slots of 30 minutes between 9 and 12
            var slots = TimeSlotFactory.Instance.Generate((new DateTime(2020, 1, 1, 9, 0, 0), new DateTime(2020, 1, 1, 12, 0, 0), TimeSpan.FromMinutes(30)))
                .ToArray();
            slots.Length.Should().Be(6);

            // Generate time slots of 1 hour between 9 and 12
            slots = TimeSlotFactory.Instance.Generate((new DateTime(2020, 1, 1, 9, 0, 0), new DateTime(2020, 1, 1, 12, 0, 0), TimeSpan.FromHours(1)))
                .ToArray();
            slots.Length.Should().Be(3);

            // Generate time slots of 1 hour between 9 and 12 and of 30 minutes between 14 and 20
            slots = TimeSlotFactory.Instance.Generate(
                (new DateTime(2020, 1, 1, 9, 0, 0), new DateTime(2020, 1, 1, 12, 0, 0), TimeSpan.FromHours(1)),
                (new DateTime(2020, 1, 1, 14, 0, 0), new DateTime(2020, 1, 1, 20, 0, 0), TimeSpan.FromMinutes(30)))
                .ToArray();
            // Expect 3 + (6 * 2)
            slots.Length.Should().Be(15);
        }
    }
}