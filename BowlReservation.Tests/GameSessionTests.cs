﻿using BowlReservation.ViewModels;
using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace BowlReservation.Tests
{
    public class GameSessionTests
    {
        [Fact]
        public void TestSession()
        {
            LaneSlotFactory.Instance.MaxLanes = 12;
            var laneSlots = LaneSlotFactory.Instance.Generate();
            var timeSlots = TimeSlotFactory.Instance.Generate((new DateTime(2020, 1, 1, 9, 0, 0), new DateTime(2020, 1, 1, 12, 0, 0), TimeSpan.FromMinutes(30)));

            var date = new DateTime(2020, 1, 1);
            var sessions = laneSlots.SelectMany(ls => timeSlots.Select(ts => new GameSession(Guid.Parse("59EE3BEF-0646-4C68-B689-2A9AC1AE8073"), date, ts, ls))).ToArray();
            sessions.Length.Should().Be(72);
        }
    }
}