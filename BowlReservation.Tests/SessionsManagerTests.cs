﻿using BowlReservation.ViewModels;
using DynamicData;
using FluentAssertions;
using Microsoft.Reactive.Testing;
using MoreLinq;
using ReactiveUI.Testing;
using System;
using System.Linq;
using Xunit;

namespace BowlReservation.Tests
{
    public class SessionsManagerTests
    {
        [Fact]
        public void TestStatus() => new TestScheduler().With(scheduler =>
        {
            scheduler.Start();

            LaneSlotFactory.Instance.MaxLanes = 12;
            var laneSlots = LaneSlotFactory.Instance.Generate();
            var timeSlots = TimeSlotFactory.Instance.Generate((new DateTime(2020, 1, 1, 9, 0, 0), new DateTime(2020, 1, 1, 12, 0, 0), TimeSpan.FromMinutes(30)));

            var sessionsManager = new SessionsManager(Guid.Parse("057630CF-EF3C-4A0C-A9A8-002829B01E7E"), scheduler);
            var date = new DateTime(2020, 1, 1);
            sessionsManager.SessionsCache.AddOrUpdate(laneSlots.SelectMany(ls => timeSlots.Select(ts => new GameSession(Guid.Parse("59EE3BEF-0646-4C68-B689-2A9AC1AE8073"), date, ts, ls))));

            // Manager isn't activated
            sessionsManager.Sessions.Should().BeNull();
            sessionsManager.Activator.Activate();

            scheduler.AdvanceByMs(100);

            sessionsManager.Sessions.Count.Should().Be(72);

            sessionsManager.FreeSessions.Count.Should().Be(72);

            sessionsManager.FreeSessions.FirstOrDefault(s => s.TimeSlot.Id == new DateTime(2020, 1, 1, 9, 0, 0).Ticks)
                .Status = SessionStatus.Busy;

            scheduler.AdvanceByMs(100);

            sessionsManager.FreeSessions.Count.Should().Be(71);
            sessionsManager.FreeSessions.Where(s => s.TimeSlot.Id == new DateTime(2020, 1, 1, 9, 0, 0).Ticks).Count().Should().Be(11);
        });

        [Fact]
        public void TestAvailability() => new TestScheduler().With(scheduler =>
        {
            scheduler.Start();

            LaneSlotFactory.Instance.MaxLanes = 12;
            var laneSlots = LaneSlotFactory.Instance.Generate();
            var timeSlots = TimeSlotFactory.Instance.Generate((new DateTime(2020, 1, 1, 9, 0, 0), new DateTime(2020, 1, 1, 12, 0, 0), TimeSpan.FromMinutes(30)));

            var sessionsManager = new SessionsManager(Guid.Parse("057630CF-EF3C-4A0C-A9A8-002829B01E7E"), scheduler);
            sessionsManager.Activator.Activate();
            scheduler.AdvanceByMs(100);

            var date = new DateTime(2020, 1, 1);
            sessionsManager.SessionsCache.AddOrUpdate(laneSlots.SelectMany(ls => timeSlots.Select(ts => new GameSession(Guid.Parse("59EE3BEF-0646-4C68-B689-2A9AC1AE8073"), date, ts, ls))));
            scheduler.AdvanceByMs(100);

            // Set 10 lanes out of 12 to busy at 9:30
            sessionsManager.FreeSessions.Where(s => s.TimeSlot.Id == new DateTime(2020, 1, 1, 9, 30, 0).Ticks)
                .Take(10)
                .ForEach(s => s.Status = SessionStatus.Busy);
            // Set 11 lanes out of 12 to busy at 10:00
            sessionsManager.FreeSessions.Where(s => s.TimeSlot.Id == new DateTime(2020, 1, 1, 10, 0, 0).Ticks)
                .Take(11)
                .ForEach(s => s.Status = SessionStatus.Busy);

            scheduler.AdvanceByMs(100);

            // Need one lane
            var hasSlots = sessionsManager.HasAvailabilities(new DateTime(2020, 1, 1, 9, 25, 0), 4);
            hasSlots.Should().BeTrue();

            // Need two lanes
            hasSlots = sessionsManager.HasAvailabilities(new DateTime(2020, 1, 1, 9, 25, 0), 6, maxPlayersPerLane: 4);
            hasSlots.Should().BeTrue();

            // Need three lanes
            hasSlots = sessionsManager.HasAvailabilities(new DateTime(2020, 1, 1, 9, 25, 0), 9, maxPlayersPerLane: 4);
            hasSlots.Should().BeFalse();

            // Need one lane for 2 timeslots
            hasSlots = sessionsManager.HasAvailabilities(new DateTime(2020, 1, 1, 9, 25, 0), 4, 2, 4);
            hasSlots.Should().BeTrue();

            // Need two lanes for 2 timeslots
            hasSlots = sessionsManager.HasAvailabilities(new DateTime(2020, 1, 1, 9, 25, 0), 6, 2, maxPlayersPerLane: 4);
            hasSlots.Should().BeFalse();

            scheduler.AdvanceBy(TimeSpan.FromHours(3).Ticks);
            sessionsManager.FreeSessions.Count.Should().Be(0);
        });

        [Fact]
        public void TestBooking() => new TestScheduler().With(scheduler =>
        {
            scheduler.Start();

            LaneSlotFactory.Instance.MaxLanes = 12;
            var laneSlots = LaneSlotFactory.Instance.Generate();
            var timeSlots = TimeSlotFactory.Instance.Generate((new DateTime(2020, 1, 1, 9, 0, 0), new DateTime(2020, 1, 1, 12, 0, 0), TimeSpan.FromMinutes(30)));

            var sessionsManager = new SessionsManager(Guid.Parse("057630CF-EF3C-4A0C-A9A8-002829B01E7E"), scheduler);
            sessionsManager.Activator.Activate();
            scheduler.AdvanceByMs(100);

            var date = new DateTime(2020, 1, 1);
            sessionsManager.SessionsCache.AddOrUpdate(laneSlots.SelectMany(ls => timeSlots.Select(ts => new GameSession(Guid.Parse("59EE3BEF-0646-4C68-B689-2A9AC1AE8073"), date, ts, ls)))); ;
            scheduler.AdvanceByMs(100);

            // Set 10 lanes out of 12 to busy at 9:30
            sessionsManager.FreeSessions.Where(s => s.TimeSlot.Id == new DateTime(2020, 1, 1, 9, 30, 0).Ticks)
                .Take(10)
                .ForEach(s => s.Status = SessionStatus.Busy);
            // Set 11 lanes out of 12 to busy at 10:00
            sessionsManager.FreeSessions.Where(s => s.TimeSlot.Id == new DateTime(2020, 1, 1, 10, 0, 0).Ticks)
                .Take(11)
                .ForEach(s => s.Status = SessionStatus.Busy);

            scheduler.AdvanceByMs(100);

            var booking = sessionsManager.Book("Dummy", new DateTime(2020, 1, 1, 9, 25, 0), 4);
            scheduler.AdvanceByMs(100);

            sessionsManager.FreeSessions.Where(s => s.TimeSlot.Id == new DateTime(2020, 1, 1, 9, 30, 0).Ticks)
                .Count().Should().Be(1);
        });
    }
}