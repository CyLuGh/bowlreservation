﻿using BowlReservation.ViewModels;
using FluentAssertions;
using System;
using System.Linq;
using Xunit;

namespace BowlReservation.Tests
{
    public class OpeningSettingsTests
    {
        [Fact]
        public void TestSchedules()
        {
            // Define regular opening schedules
            var monday = new RegularOpeningSettings(DayOfWeek.Monday, new Schedule(new TimeSpan(9, 0, 0), new TimeSpan(20, 0, 0), TimeSpan.FromMinutes(30)));
            // Closed on tuesday!
            //var tuesday = new RegularOpeningSettings(DayOfWeek.Tuesday, (new TimeSpan(9, 0, 0), new TimeSpan(20, 0, 0), TimeSpan.FromMinutes(30)));
            var wednesday = new RegularOpeningSettings(DayOfWeek.Wednesday, new Schedule(new TimeSpan(9, 0, 0), new TimeSpan(20, 0, 0), TimeSpan.FromMinutes(30)));
            var thursday = new RegularOpeningSettings(DayOfWeek.Thursday, new Schedule(new TimeSpan(9, 0, 0), new TimeSpan(20, 0, 0), TimeSpan.FromMinutes(30)));
            var friday = new RegularOpeningSettings(DayOfWeek.Friday,
                new Schedule(new TimeSpan(9, 0, 0), new TimeSpan(12, 0, 0), TimeSpan.FromMinutes(60)),
                new Schedule(new TimeSpan(12, 0, 0), new TimeSpan(26, 0, 0), TimeSpan.FromMinutes(30)));
            var saturday = new RegularOpeningSettings(DayOfWeek.Saturday, new Schedule(new TimeSpan(9, 0, 0), new TimeSpan(26, 0, 0), TimeSpan.FromMinutes(30)));
            var sunday = new RegularOpeningSettings(DayOfWeek.Sunday, new Schedule(new TimeSpan(9, 0, 0), new TimeSpan(18, 0, 0), TimeSpan.FromMinutes(30)));

            // Define special opening day
            var specialDay = new ExceptionalOpeningSettings(new DateTime(2020, 5, 12), new Schedule(new TimeSpan(9, 0, 0), new TimeSpan(20, 0, 0), TimeSpan.FromMinutes(30)));
            var closedDay = new ExceptionalOpeningSettings(new DateTime(2020, 5, 14));

            var settings = new OpeningSettings[] {
                monday,
                wednesday,
                thursday,
                friday,
                saturday,
                sunday,
                specialDay,
                closedDay
            };

            // Monday
            new DateTime(2020, 5, 4).GetTimeSlots(settings).Count().Should().Be(22);

            // Tuesday
            new DateTime(2020, 5, 5).GetTimeSlots(settings).Count().Should().Be(0);

            // Wednesday
            new DateTime(2020, 5, 6).GetTimeSlots(settings).Count().Should().Be(22);

            // Thursday
            new DateTime(2020, 5, 7).GetTimeSlots(settings).Count().Should().Be(22);

            // Friday
            new DateTime(2020, 5, 8).GetTimeSlots(settings).Count().Should().Be(31);

            // Saturday
            new DateTime(2020, 5, 9).GetTimeSlots(settings).Count().Should().Be(34);

            // Sunday
            new DateTime(2020, 5, 10).GetTimeSlots(settings).Count().Should().Be(18);

            // Tuesday -> should be closed but a special setting should kick in
            new DateTime(2020, 5, 12).GetTimeSlots(settings).Count().Should().Be(22);

            // Thursday -> opened but a special setting should flag it as closed
            new DateTime(2020, 5, 14).GetTimeSlots(settings).Count().Should().Be(0);
        }

        [Fact]
        public void TestExceptions()
        {
            Assert.Throws<ArgumentException>(() => new RegularOpeningSettings(DayOfWeek.Monday, new Schedule(TimeSpan.FromHours(10), TimeSpan.FromHours(9), TimeSpan.FromMinutes(30))));

            Assert.Throws<ArgumentException>(() => new RegularOpeningSettings(DayOfWeek.Monday,
                new Schedule(TimeSpan.FromHours(9), TimeSpan.FromHours(13), TimeSpan.FromMinutes(30)),
                new Schedule(TimeSpan.FromHours(12), TimeSpan.FromHours(20), TimeSpan.FromMinutes(30))));
        }
    }
}