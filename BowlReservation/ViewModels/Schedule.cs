﻿using System;

namespace BowlReservation.ViewModels
{
    public class Schedule
    {
        public TimeSpan Begin { get; }
        public TimeSpan End { get; }
        public TimeSpan Duration { get; }

        public Schedule(TimeSpan begin, TimeSpan end, TimeSpan duration)
        {
            if (end < begin)
                throw new ArgumentException("A schedule has an end time defined sooner than it's start time");

            Begin = begin;
            End = end;
            Duration = duration;
        }
    }
}