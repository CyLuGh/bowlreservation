﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BowlReservation.ViewModels
{
    /// <summary>
    /// Represents a lane availability
    /// </summary>
    public struct LaneSlot
    {
        public int Id { get; set; }

        public LaneSlot(int id)
        {
            Id = id;
        }
    }

    public sealed class LaneSlotFactory
    {
        public static LaneSlotFactory Instance { get; } = new LaneSlotFactory();

        public int MaxLanes { get; set; }

        private LaneSlotFactory()
        {
        }

        public IEnumerable<LaneSlot> Generate()
            => Generate(LaneSlotFactoryRules.All);

        public IEnumerable<LaneSlot> Generate(Predicate<LaneSlot> rule)
            => Enumerable.Range(0, MaxLanes)
                         .Select(i => new LaneSlot(i + 1))
                         .Where(ls => rule(ls));
    }

    public static class LaneSlotFactoryRules
    {
        public static Predicate<LaneSlot> All { get; } = _ => true;
        public static Predicate<LaneSlot> Half { get; } = ls => ls.Id % 2 == 1;
        public static Predicate<LaneSlot> None { get; } = _ => false;
    }
}