﻿using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace BowlReservation.ViewModels
{
    public enum SessionStatus { Free = 0, Busy = 1, Tentative = 2 }

    public class GameSession : ReactiveObject
    {
        public Guid Bowling { get; }
        public DateTime Date { get; }
        public TimeSlot TimeSlot { get; }
        public LaneSlot LaneSlot { get; }

        [Reactive] public SessionStatus Status { get; set; } = SessionStatus.Free;

        public GameSession(Guid bowling, DateTime date, TimeSlot timeSlot, LaneSlot laneSlot)
        {
            Bowling = bowling;
            Date = date;
            TimeSlot = timeSlot;
            LaneSlot = laneSlot;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = TimeSlot.Id.GetHashCode();
                hashCode = (hashCode * 397) ^ LaneSlot.Id.GetHashCode();
                return hashCode;
            }
        }

        public override string ToString()
            => $"LaneSlot {LaneSlot.Id} - TimeSlot {TimeSlot.Id} ({new DateTime(TimeSlot.Id)})";
    }
}