﻿using System;
using System.Linq;

namespace BowlReservation.ViewModels
{
    public abstract class OpeningSettings
    {
        public Schedule[] Schedules { get; }

        protected OpeningSettings(params Schedule[] schedules)
        {
            for (int i = 0; i < schedules.Length; i++)
            {
                var current = schedules[i];
                for (int j = i + 1; j < schedules.Length; j++)
                {
                    var other = schedules[j];
                    if (current.End > other.Begin)
                        throw new ArgumentException("Schedules are overlapping!");
                }
            }

            Schedules = schedules;
        }
    }

    public class RegularOpeningSettings : OpeningSettings
    {
        public DayOfWeek DayOfWeek { get; }

        public RegularOpeningSettings(DayOfWeek dayOfWeek, params Schedule[] schedules)
            : base(schedules)
        {
            DayOfWeek = dayOfWeek;
        }
    }

    public class ExceptionalOpeningSettings : OpeningSettings
    {
        public DateTime Date { get; }

        public ExceptionalOpeningSettings(DateTime date, params Schedule[] schedules)
            : base(schedules)
        {
            Date = date;
        }
    }
}