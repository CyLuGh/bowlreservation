﻿using DynamicData;
using MoreLinq;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reactive.Concurrency;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Text;
using System.Threading;

namespace BowlReservation.ViewModels
{
    public class SessionsManager : ReactiveObject, IActivatableViewModel
    {
        private readonly Mutex _bookMutex = new Mutex();

        // TODO replace
        public Guid Organization { get; }

        public SourceCache<GameSession, (Guid, int, long)> SessionsCache { get; }
            = new SourceCache<GameSession, (Guid, int, long)>(s => (s.Bowling, s.LaneSlot.Id, s.TimeSlot.Id));

        private ReadOnlyObservableCollection<GameSession> _sessions;
        public ReadOnlyObservableCollection<GameSession> Sessions => _sessions;

        private ReadOnlyObservableCollection<GameSession> _freeSessions;
        public ReadOnlyObservableCollection<GameSession> FreeSessions => _freeSessions;

        private ReadOnlyObservableCollection<(Guid, DateTime)> _loadedDates;
        public ReadOnlyObservableCollection<(Guid, DateTime)> LoadedDates => _loadedDates;

        public ViewModelActivator Activator { get; }

        public int MaxPlayersPerLane { get; set; } = 8;

        public SessionsManager(Guid organisation, IScheduler scheduler = null)
        {
            Activator = new ViewModelActivator();
            Organization = organisation;
            scheduler ??= RxApp.MainThreadScheduler;

            this.WhenActivated(disposables =>
            {
                var sessions = SessionsCache.Connect()
                    .ExpireAfter(_ => TimeSpan.FromHours(2), TimeSpan.FromMinutes(5), scheduler);

                sessions
                    .ObserveOn(scheduler)
                    .Bind(out _sessions)
                    .DisposeMany()
                    .Subscribe()
                    .DisposeWith(disposables);

                sessions
                    .AutoRefresh(x => x.Status)
                    .Filter(s => s.Status == SessionStatus.Free)
                    .ObserveOn(scheduler)
                    .Bind(out _freeSessions)
                    .DisposeMany()
                    .Subscribe()
                    .DisposeWith(disposables);

                sessions
                    .Transform(o => (o.Bowling, o.Date))
                    .Distinct()
                    .ObserveOn(scheduler)
                    .Bind(out _loadedDates)
                    .DisposeMany()
                    .Subscribe()
                    .DisposeWith(disposables);
            });
        }

        private TimeSlot FindTimeSlot(DateTime start)
            => Sessions.Select(o => o.TimeSlot)
                .Distinct()
                .Where(o => o.Begin >= start).Min();

        private TimeSlot[] FindTimeSlots(DateTime start, int consecutiveGames = 1)
        {
            var timeSlots = new List<TimeSlot>
            {
                FindTimeSlot(start)
            };

            Enumerable.Range(1, consecutiveGames - 1)
                .Aggregate(timeSlots, (l, _) =>
                {
                    l.Add(FindTimeSlot(timeSlots.Max().End));
                    return l;
                });
            return timeSlots.ToArray();
        }

        public bool HasAvailabilities(DateTime start, int players, int consecutiveGames = 1, int maxPlayersPerLane = 0)
        {
            var timeSlots = FindTimeSlots(start, consecutiveGames);
            return HasAvailabilities(timeSlots, players, maxPlayersPerLane);
        }

        private bool HasAvailabilities(TimeSlot[] timeSlots, int players, int maxPlayersPerLane = 0)
        {
            var availableSessions = timeSlots.Select(ts => FreeSessions.Where(o => o.TimeSlot.Equals(ts)));
            maxPlayersPerLane = maxPlayersPerLane != 0 ? maxPlayersPerLane : MaxPlayersPerLane;

            var neededLanes = (int)Math.Ceiling((decimal)players / maxPlayersPerLane);
            return availableSessions.All(x => x.Count() >= neededLanes);
        }

        public Booking Book(string client, DateTime start, int players, int consecutiveGames = 1, int maxPlayersPerLane = 0)
        {
            maxPlayersPerLane = maxPlayersPerLane != 0 ? maxPlayersPerLane : MaxPlayersPerLane;
            var neededLanes = (int)Math.Ceiling((decimal)players / maxPlayersPerLane);

            _bookMutex.WaitOne();

            var timeSlots = FindTimeSlots(start, consecutiveGames);

            var sessionsGroup = timeSlots.Select(ts => FreeSessions.Where(o => o.TimeSlot.Equals(ts)).Take(neededLanes)).ToArray();
            if (sessionsGroup.All(g => g.Count() == neededLanes))
            {
                var sessions = sessionsGroup.SelectMany(o => o).Select(s =>
                {
                    s.Status = SessionStatus.Tentative;
                    return s;
                }).ToArray();

                _bookMutex.ReleaseMutex();

                return new Booking
                {
                    Client = client,
                    Players = players,
                    Sessions = sessions
                };
            }
            else
            {
                _bookMutex.ReleaseMutex();
                return null; // TODO replace with Option
            }
        }
    }
}