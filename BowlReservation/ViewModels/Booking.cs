﻿using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace BowlReservation.ViewModels
{
    public class Booking : ReactiveObject
    {
        [Reactive] public ulong Id { get; set; }
        [Reactive] public GameSession[] Sessions { get; set; }
        [Reactive] public string Client { get; set; } // TODO: replace
        [Reactive] public int Players { get; set; } // TODO: replace
    }
}