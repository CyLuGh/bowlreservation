﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace BowlReservation.ViewModels
{
    /// <summary>
    /// Represents a period availability
    /// </summary>
    public struct TimeSlot : IEquatable<TimeSlot>, IComparable<TimeSlot>
    {
        public static TimeSlot Default = new TimeSlot(0, TimeSpan.FromMilliseconds(0));

        public long Id { get; set; }
        public TimeSpan Duration { get; set; }

        public DateTime Begin => new DateTime(Id);
        public DateTime End => Begin.Add(Duration);

        public TimeSlot(long id, TimeSpan duration)
        {
            Id = id;
            Duration = duration;
        }

        public override int GetHashCode()
            => Id.GetHashCode();

        public bool Equals(TimeSlot other)
        {
            if (ReferenceEquals(null, other))
                return false;
            if (ReferenceEquals(this, other))
                return true;

            return Id.Equals(other.Id);
        }

        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
                return false;

            return Equals((TimeSlot)obj);
        }

        public int CompareTo(TimeSlot other)
            => Id.CompareTo(other.Id);
    }

    public sealed class TimeSlotFactory
    {
        public static TimeSlotFactory Instance { get; } = new TimeSlotFactory();

        private TimeSlotFactory()
        {
        }

        public IEnumerable<TimeSlot> Generate(params (DateTime begin, DateTime end, TimeSpan duration)[] schedules)
        {
            foreach (var setting in schedules)
            {
                var (begin, end, duration) = setting;

                var currentStart = begin;

                while (currentStart < end)
                {
                    var currentEnd = currentStart.Add(duration);

                    if (currentEnd <= end)
                        yield return new TimeSlot(currentStart.Ticks, duration);

                    currentStart = currentEnd;
                }
            }
        }

        public IEnumerable<TimeSlot> Generate(DateTime date, params Schedule[] schedules)
        {
            foreach (var schedule in schedules)
            {
                var currentStart = schedule.Begin;

                while (currentStart < schedule.End)
                {
                    var currentEnd = currentStart.Add(schedule.Duration);

                    if (currentEnd <= schedule.End)
                        yield return new TimeSlot(date.Add(currentStart).Ticks, schedule.Duration);

                    currentStart = currentEnd;
                }
            }
        }
    }

    public static class TimeSlotExtensions
    {
        public static IEnumerable<TimeSlot> GetTimeSlots(this DateTime date, IEnumerable<OpeningSettings> settings)
        {
            OpeningSettings setting = settings.OfType<ExceptionalOpeningSettings>().FirstOrDefault(x => x.Date.Equals(date));
            if (setting != null)
                return TimeSlotFactory.Instance.Generate(date, setting.Schedules);

            setting = settings.OfType<RegularOpeningSettings>().FirstOrDefault(x => x.DayOfWeek.Equals(date.DayOfWeek));
            if (setting != null)
                return TimeSlotFactory.Instance.Generate(date, setting.Schedules);

            return new TimeSlot[0];
        }
    }
}